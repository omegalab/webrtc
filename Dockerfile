FROM node:alpine
ENV TZ=Europe/Moscow
WORKDIR /usr/src/app
COPY . .
RUN apk --no-cache add make g++ python ncurses && npm install --progress=false && npm run build && apk del --no-cache make g++ python
EXPOSE 3000
CMD [ "npm", "run" ,"start" ]
